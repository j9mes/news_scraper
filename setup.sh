#! /bin/bash

# Script to create a virtual environment and install the python
# dependencies required by this project.

virtualenv -p python3 venv
venv/bin/pip install -r requirements.txt

