An application which pulls the latest top news stories from RNZ, NZH and Stuff
and lists them on screen. 

Colour coded by news source.

Checks and updates immediately if anything changes.

## Installation

Run the set-up script to create a virtual environment and install
the required dependencies using:
```
./setup.sh
```
If you're having permission errors, make sure the script is executable
using:
```
chmod u+x setup.sh
```

