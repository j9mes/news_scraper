from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
from random import shuffle
from datetime import datetime


class BColors:

    STUFF = '\033[92m'
    NZH = '\u001b[37m'
    RNZ = '\033[93m'
    ENDC = '\033[0m'
    TIME = '\u001b[35'

    # HEADER = '\033[95m'
    # BOLD = '\033[1m'
    # UNDERLINE = '\033[4m'
    #
    # Black: \u001b[30
    # Red: \u001b[31
    # Green: \u001b[32
    # Yellow: \u001b[33
    # Blue: \u001b[34
    # Magenta: \u001b[35
    # Cyan: \u001b[36
    # White: \u001b[37
    # Reset: \u001b[0


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors.
    This function just prints them, but you can
    make it do anything.
    """
    print(e)


def slugger():

    all_stories = []
    all_other_stories = []
    all_lead_stories = []
    all_top_stories = []

    for story in get_rnz_headlines():
        all_stories.append(story)
    for story in get_nzh_headlines():
        all_stories.append(story)
    for story in get_stuff_headlines():
        all_stories.append(story)

    print(datetime.now().strftime("%H:%M:%S"))

    for slug in all_stories:
        if str(slug).startswith('**'):
            all_lead_stories.append(slug)
        elif str(slug).startswith('*'):
            all_top_stories.append(slug)
        else:
            all_other_stories.append(slug)

    shuffle(all_lead_stories)
    shuffle(all_top_stories)
    shuffle(all_other_stories)

    for slug in all_lead_stories:
        if slug.endswith('(RNZ)'):
            print(BColors.RNZ + slug + BColors.ENDC)
        elif slug.endswith('(NZH)'):
            print(BColors.NZH + slug + BColors.ENDC)
        elif slug.endswith('(Stuff)'):
            print(BColors.STUFF + slug + BColors.ENDC)

    for slug in all_top_stories:
        if slug.endswith('(RNZ)'):
            print(BColors.RNZ + slug + BColors.ENDC)
        elif slug.endswith('(NZH)'):
            print(BColors.NZH + slug + BColors.ENDC)
        elif slug.endswith('(Stuff)'):
            print(BColors.STUFF + slug + BColors.ENDC)

    for slug in all_other_stories:
        if slug.endswith('(RNZ)'):
            print(BColors.RNZ + slug + BColors.ENDC)
        elif slug.endswith('(NZH)'):
            print(BColors.NZH + slug + BColors.ENDC)
        elif slug.endswith('(Stuff)'):
            print(BColors.STUFF + slug + BColors.ENDC)

    while True:
        all_stories2 = []
        all_lead_stories = []
        all_top_stories = []
        all_other_stories = []

        for story in get_rnz_headlines():
            all_stories2.append(story)
        for story in get_nzh_headlines():
            all_stories2.append(story)
        for story in get_stuff_headlines():
            all_stories2.append(story)

        if all_stories == all_stories2:

            pass

        else:
            print(chr(27) + "[2J")  # Clear the console
            print(datetime.now().strftime("%H:%M:%S"))

            for slug in all_stories2:
                if str(slug).startswith('**'):
                    all_lead_stories.append(slug)
                elif str(slug).startswith('*'):
                    all_top_stories.append(slug)
                else:
                    all_other_stories.append(slug)

            shuffle(all_lead_stories)
            shuffle(all_top_stories)
            shuffle(all_other_stories)

            for slug in all_lead_stories:
                if slug.endswith('(RNZ)'):
                    print(BColors.RNZ + slug + BColors.ENDC)
                elif slug.endswith('(NZH)'):
                    print(BColors.NZH + slug + BColors.ENDC)
                elif slug.endswith('(Stuff)'):
                    print(BColors.STUFF + slug + BColors.ENDC)

            for slug in all_top_stories:
                if slug.endswith('(RNZ)'):
                    print(BColors.RNZ + slug + BColors.ENDC)
                elif slug.endswith('(NZH)'):
                    print(BColors.NZH + slug + BColors.ENDC)
                elif slug.endswith('(Stuff)'):
                    print(BColors.STUFF + slug + BColors.ENDC)

            for slug in all_other_stories:
                if slug.endswith('(RNZ)'):
                    print(BColors.RNZ + slug + BColors.ENDC)
                elif slug.endswith('(NZH)'):
                    print(BColors.NZH + slug + BColors.ENDC)
                elif slug.endswith('(Stuff)'):
                    print(BColors.STUFF + slug + BColors.ENDC)

            all_stories = all_stories2


def get_rnz_headlines():
    rnz_url = 'https://www.rnz.co.nz/news'
    rnz_response = simple_get(rnz_url)
    rnz_headlines = []

    if rnz_response is not None:
        rnz = BeautifulSoup(rnz_response, 'html.parser')

        rnz_headlines.append('** ' + (rnz.find('div', class_='lead-story').find_next('h3', class_='o-digest__headline')  .text.strip()) + ' (RNZ)')

        rnz_top_stories = rnz.find('ul', class_='c-top-stories__list').find_all('h3', class_='o-digest__headline')
        for rnz_top_story in rnz_top_stories:
            rnz_headlines.append('* ' + rnz_top_story.text.strip() + ' (RNZ)')

        rnz_secondary_stories = rnz.find('ul', class_='c-top-stories__secondary-list').find_all('h3', class_='o-digest__headline')
        for rnz_secondary_story in rnz_secondary_stories:
            rnz_headlines.append(rnz_secondary_story.text.strip() + ' (RNZ)')

        return rnz_headlines

    raise Exception('Error retrieving RNZ contents at {}'.format(rnz_url))


def get_nzh_headlines():
    nzh_url = 'https://www.nzherald.co.nz/'
    nzh_response = simple_get(nzh_url)
    nzh_headlines = []

    if nzh_response is not None:

        nzh = BeautifulSoup(nzh_response, 'html.parser')

        nzh_lead_story = nzh.find_all('article', class_='story-hero')
        nzh_headlines.append('** ' + (nzh_lead_story[0].find('h3').text.strip()) + ' (NZH)')

        nzh_top_stories = nzh.find_all('div', class_='rightone')
        for nzh_top_story in nzh_top_stories:
            nzh_headlines.append('* ' + nzh_top_story.find('h3').text.strip() + ' (NZH)')

        nzh_secondary_stories = nzh.find('div', class_='last').find_all('h3')
        for nzh_secondary_story in nzh_secondary_stories:
            nzh_headlines.append(nzh_secondary_story.text.strip() + ' (NZH)')

        return nzh_headlines

    raise Exception('Error retrieving NZH contents at {}'.format(nzh_url))


def get_stuff_headlines():
    stuff_url = 'https://www.stuff.co.nz/'
    stuff_response = simple_get(stuff_url)
    stuff_headlines = []

    if stuff_response is not None:
        stuff = BeautifulSoup(stuff_response, 'html.parser')

        stuff_lead_story = stuff.find('h1', class_='portrait__main-article-headline')
        stuff_headlines.append('** ' + stuff_lead_story.text.strip() + ' (Stuff)')

        stuff_second_lead_stories = stuff.find('div', class_='esi-parsys section').find_all('h3', class_='first_headline')
        for stuff_lead_story in stuff_second_lead_stories:
            stuff_headlines.append('* ' + stuff_lead_story.text.strip() + ' (Stuff)')

        stuff_secondary_stories = stuff.find('div', class_='esi-parsys').find_all('h3', class_='other_headline')
        for stuff_secondary_story in stuff_secondary_stories:
            stuff_headlines.append(stuff_secondary_story.text.strip() + ' (Stuff)')

        return stuff_headlines

    raise Exception('Error retrieving Stuff contents at {}'.format(stuff_url))


slugger()
